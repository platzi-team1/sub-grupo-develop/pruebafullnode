const express = require('express')
const app = express()
const cors = require('cors')
app.use(cors())

// Para parsear los request
app.use(express.json())

// Para colocar los logger como middleware
// middleware es una funcion que intersecta la peticion que esta pasando por el api
const logger = require('./loggerMiddleware')
app.use(logger)

//  const { v4: uuidv4, v4 } = require('uuid')

/**
 * ! eslint
 * ? mpm i eslint -D ==> esta se instala en dependencia de desarrollo
 * * se debe tambien instalar  la extencion ESLint en visualcode para que detecte el archivo .eslint
 * * Opcionalmente de instala la extencion Error Lens para que locoloque los errores en cada linea
 * * Para inicializar la configuracion ejecutamos el comando de slint
 * ? ./node_modules/.bin/eslint --init
 * * Con este comando nos detecta los errores
 * ? ./node_modules/.bin/eslint .
 * * Tambien podemos colocar en el script loa anterior de esta manera
 * ? "lint": "eslint ."   ===>  npm run lint
 * * colocamos en el archivo de configuracion .eslinttrc.js en env
 * ? 'node' : true ===> para poder arreglar lo de la configuracion de browser
 * ! standard  ==> utilizapor detras eslint asi que podemos quitar del package.json esLint
 * ? mpm i standard -D ==> esta se instala en dependencia de desarrollo
 * * Se debe colocar la siguiente configuracion del linter standard en el package.json
 * ? "eslintConfig": {
 * ?      "extends": "./node_modules/standard/eslintrc.json"
 * ? }
 */

// const http = require('http');
/**
 * Paginas de interes
 * https://fullstackopen.com/en/
 * https://midu.dev/
 * https://developer.mozilla.org/es/docs/Web/HTTP/Basics_of_HTTP/MIME_types/Common_types     Lista de Content-Type
 * https://http.cat/           status code ejemplo 200, 400
 * https://www.youtube.com/watch?v=o85OkeVtm7k&list=PLV8x_i1fqBw0Kn_fBIZTa3wS_VZAqddX7&index=6
 * https://github.com/midudev/notes-api/blob/main/index.js    repo del proyecto
 *
 */

let notes = [{
  id: 1,
  content: 'Me tengo que ir',
  date: '2019-05-30T17:30:31.098Z',
  important: true
}, {
  id: 2,
  content: 'Tengo que estudiar mucho nodejs',
  date: '2019-05-30T18:39:34.091Z',
  important: false
}, {
  id: 3,
  content: 'Tengo que repasar todps los dias el codigo de nodejs',
  date: '2019-05-30T18:20:14.298Z',
  important: true
}]

/* const app = http.createServer((request, response) => {
    response.writeHead(200, { 'Content-Type': 'application/json' });
    // JSON.stringify recorre todo el array y va convirtiendo todos los datos y objetos en un string
    response.end(JSON.stringify(notes));
}); */

// get http://localhost:3001
app.get('/', (request, response) => {
  response.send('<h1>Hola mundo<h1>')
})

// get ==> http://localhost:3001/api/notes
app.get('/api/notes', (request, response) => {
  response.json(notes)
})

// get ==> http://localhost:3001/api/notes/2
app.get('/api/notes/:id', (request, response) => {
  const id = Number(request.params.id) // en los params siempre nos regresa string hay que onvertir a number
  const note = notes.find((note) => note.id === id) // Buscamos el id
  // Si trae note con el id devolvemos la note
  if (note) {
    response.json(note)
  } else {
  // Si no la encuentra devolvemos un estatus 404
    response.status(404).end()
  }
})
// delete ==> http://localhost:3001/api/notes/2
app.delete('/api/notes/:id', (request, response) => {
  const id = Number(request.params.id) // en los params siempre nos regresa string hay que onvertir a number
  notes = notes.filter((note) => note.id !== id) // Buscamos el id
  response.status(204).end()
})

app.post('/api/notes', (request, response) => {
  const { content, important } = request.body

  if (!request.body || !content) {
    return response.status(400).json({
      error: 'content is missing'
    })
  }

  // nos traemos todos los id de la notas
  const ids = notes.map((note) => note.id)
  // buscamos el maximo id
  const maxId = Math.max(...ids)

  const newNote = {
    id: maxId + 1, // Colocamos un nuevo id sumandole 1 al maximo
    content,
    important: typeof important !== 'undefined' ? important : false,
    date: new Date().toISOString()
  }

  // Guardamos la nueva nota despues de las demas notas
  notes = [...notes, newNote]

  // id: v4()
  console.log(newNote)

  response.status(201).json(newNote)
})

// Con este middelware controlamos una ruta que no este
app.use((request, response) => {
  // Aqui podemos mirar si path esta yendo al 404
  console.log(request.path)
  response.status(404).json({
    error: 'Not found'
  })
})

// Se coloca process.env.PORT para que heroku coloque el puerto que le quiera asignar
// const PORT = process.env.PORT || 3001
/* app.listen(PORT);
 console.log(`running on port: ${PORT}`); */
/* app.listen(PORT, () => {
  console.log(`running on port: ${PORT}`)
}) */

app.listen(process.env.PORT || 3001, '0.0.0.0', () => {
  console.log('Server is running.')
})
